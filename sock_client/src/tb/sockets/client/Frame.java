package tb.sockets.client;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


public class Frame extends JFrame{
	
	public JPanel contentPane;
	public JLabel lblNotConnected;
	public int port;
	public String host;
public JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField("");;
	public JButton btnConnect = new JButton("Connect");
	public JFormattedTextField frmtdtxtfldIp = new JFormattedTextField("");
	public boolean go=false;
	
	public Frame() throws Exception {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 670, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(new Color(230,230,250));
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 86, 14);
		contentPane.add(lblHost);
		
		
		
			
			frmtdtxtfldIp.setBounds(43, 14, 90, 20);
			
			
			contentPane.add(frmtdtxtfldIp);
		
		
		 
		btnConnect.setBounds(10, 70, 105, 23);
		contentPane.add(btnConnect);
		
		
		JLabel j=new JLabel("<html>Wpisz:<br/><br/>Host: localhost<br/><br/>Port: 6666</html>", SwingConstants.CENTER);
		j.setBounds(10, 300, 110, 110);
		j.setBorder(new LineBorder(Color.black, 5));
		contentPane.add(j);
		
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 39, 86, 14);
		contentPane.add(lblPort);
		
	
		btnConnect.addActionListener( new ActionListener(){

		@Override
			public void actionPerformed(ActionEvent e) {
			port=Integer.parseInt(frmtdtxtfldXxxx.getText());//jak pobrac int
				host=frmtdtxtfldIp.getText();
				
				go=true;
				
			}
			
			
		});

		
	}
	
	public Frame(String host,int port) throws Exception {
		this.host=host;this.port=port;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 670, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(new Color(230,230,250));
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 86, 14);
		contentPane.add(lblHost);
		
		
		
			
			frmtdtxtfldIp.setBounds(43, 14, 90, 20);
			frmtdtxtfldIp.setText(host);
			
			contentPane.add(frmtdtxtfldIp);
		
		
		 
		btnConnect.setBounds(10, 70, 75, 23);
		contentPane.add(btnConnect);
		
		
		
		String p=String.valueOf(port);
		
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		frmtdtxtfldXxxx.setText(p);
		contentPane.add(frmtdtxtfldXxxx);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 39, 86, 14);
		contentPane.add(lblPort);
		
	
		btnConnect.addActionListener( new ActionListener(){

		@Override
			public void actionPerformed(ActionEvent e) {
			
				
			}
			
			
		});

		
	}
}

