package tb.sockets.client;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Client extends JPanel{

	
	public JButton []b=new JButton[9];
	public static JLabel inf=new JLabel("no connection");
	public static JLabel ruch=new JLabel("");
	private static Socket socket ;
	private BufferedReader in;
	private PrintWriter out;
    public String z;
	public static String host="localhost";
	public boolean turn;
	public static int port=6666;
	
	
	public Client(String host,int port) throws IOException {
		
		try{
			
		socket = new Socket(host,port);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		setLayout(new GridLayout(3,3,0,0));
		setBackground(Color.black);
		for(int i=0;i<9;i++){
			final int r=i;
			b[i]=new JButton();
			b[i].setBackground(Color.white);
			b[i].setFont(new Font("Arial", Font.PLAIN, 160));
			b[i].addActionListener(new ActionListener() {
				

				@Override
				public void actionPerformed(ActionEvent e) {
					
					// TODO Auto-generated method stub
					if(turn==true){
					if(b[r].getText().equals("")){
					b[r].setText(z);
				
					out.println("move"+r);
					out.println("turn");}
					}
					
				}
			});add(b[i]);
		}
	}
	public static int newgame(){
		JOptionPane o=new JOptionPane();
		int a=o.showConfirmDialog(f, "new game?","",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		f.dispose();
		return a;
	}
		
	
	public void run() throws IOException{
				int move;
				String buf;
				try {
				while(true){
					
						buf=in.readLine();
						
						if(buf.startsWith("valid")){
							turn=true; ruch.setText("   Twoj ruch");
							ruch.setBackground(new Color(124,252,0));
						}
						if(buf.startsWith("notvalid")){turn=false;
						ruch.setText("  Nie twoja kolej");
						ruch.setBackground(new Color(255,0,0));
						}
						
						

						if(buf.startsWith("wincrossL-R"))//left-right cross
						{
							b[0].setBackground(Color.RED);
							b[4].setBackground(Color.RED);
							b[8].setBackground(Color.RED);
						
						}
						if(buf.startsWith("wincrossR-L"))
						{
							b[2].setBackground(Color.RED);
							b[4].setBackground(Color.RED);
							b[6].setBackground(Color.RED);	
						}
						
						if(buf.startsWith("w1"))
						{
							b[0].setBackground(Color.RED);
							b[1].setBackground(Color.RED);
							b[2].setBackground(Color.RED);	
						}
						if(buf.startsWith("w2"))
						{
							b[3].setBackground(Color.RED);
							b[4].setBackground(Color.RED);
							b[5].setBackground(Color.RED);	
						}
						if(buf.startsWith("w3"))
						{
							b[6].setBackground(Color.RED);
							b[7].setBackground(Color.RED);
							b[8].setBackground(Color.RED);	
						}
						if(buf.startsWith("w4"))
						{
							b[0].setBackground(Color.RED);
							b[3].setBackground(Color.RED);
							b[6].setBackground(Color.RED);	
						}
						if(buf.startsWith("w5"))
						{
							b[1].setBackground(Color.RED);
							b[4].setBackground(Color.RED);
							b[7].setBackground(Color.RED);	
						}
						if(buf.startsWith("w6"))
						{System.out.println("true");
							b[2].setBackground(Color.RED);
							b[5].setBackground(Color.RED);
							b[8].setBackground(Color.RED);	
						}
					
						
						if(buf.startsWith("znak"))
						{z=buf.substring(4);
						f.setTitle("GRACZ "+z);
						}
						
						if(buf.startsWith("con"))
						{
							
						inf.setText("  connected with server");
						inf.setBackground(new Color(255,215,0));
						}
						if(buf.startsWith("oponent")){
							move=Integer.parseInt(buf.substring(7));
							
							if(z.equals("X")){b[move].setText("O");}
							else{b[move].setText("X");}
						}

						if(buf.startsWith("w")){ruch.setText("  Wygrales");break;}
						if(buf.startsWith("lose")){ruch.setText("  Przegrales");break;}
						if(buf.startsWith("remis")){
							ruch.setText(" nikt nie wygral  ");
							ruch.setBackground(new Color(186,85,211));
							break;}
					
				}
					
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				finally {
				
					socket.close();
					
				
				}}
	public static int h=0,u=1;;
public static Frame f;
	
	public static void main(String[] args) throws Exception{
		
	//while(true){	
		f =new Frame();
		JPanel p=new JPanel();
		p.setBounds(145, 14, 487, 448);p.setBackground(Color.WHITE);
		f.add(p);
		f.setVisible(true);
		
	while(f.go==false)	{
		System.out.print("");
		}
	while(true){	
		if(h>0){f=new Frame(f.host,f.port);}
	Client	panel = new Client(f.host,f.port);
		
		inf.setBounds(10,120,130,30);
		panel.setBounds(145, 14, 487, 448);
		inf.setOpaque(true);
		
		ruch.setFont(new Font("Arial", Font.PLAIN, 18));
		ruch.setBounds(10, 160, 130, 50);
		ruch.setBackground(new Color(124,252,0));
		ruch.setOpaque(true);

		
		f.contentPane.add(panel);	
		f.contentPane.add(inf);	f.contentPane.add(ruch);	
		f.setVisible(true);
		

		panel.run();h++;
		if(newgame()==1){;break;}   
			
	}	
	}
	
	}
	
	
	
		
		